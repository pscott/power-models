import fops
import fops.solver
import math

def extract_value(var, x):
	return fops.Complex(var.re.val(x), var.im.val(x))

def make_complex_var(m, **kwargs):
	return fops.Complex(m.add_var(**kwargs), m.add_var(**kwargs))

def make_complex_par_polar(mag, ang):
	return fops.Complex(mag*math.cos(math.pi*ang/180.0),
			mag*math.sin(math.pi*ang/180.0))

def make_complex_var_vec(m, n=3, **kwargs):
		x = []
		for i in range(n):
			x.append(make_complex_var(m, **kwargs))
		return x

def make_complex_par_vec(n=3, init=(0.0, 0.0)):
		x = []
		for i in range(n):
			x.append(fops.Complex(*init))
		return x

def make_var_voltages(mod, mag=1.0, phase=0.0):
	x = []
	for i in range(3):
		init = make_complex_par_polar(mag, -120.0*i + phase)
		x.append(fops.Complex(
				mod.add_var(init=init.re),
				mod.add_var(init=init.im)
				))
	return x

def make_par_voltages(mag=1.0, phase=0.0):
	x = []
	for i in range(3):
		x.append(make_complex_par_polar(mag, -120.0*i + phase))
	return x

m = fops.Function()

v_p = make_var_voltages(m)
i_p = make_complex_var_vec(m)

v_s = make_var_voltages(m)
i_s = make_complex_var_vec(m)
i_s_ph = make_complex_var_vec(m)

v_g = make_par_voltages()
p = make_complex_par_vec(init=(0.1, 0.0))
p[0].re = 0.2
p[1].re = 0.2
#p[2].re = 0.145
#p[2].re = 0.07
p[2].re = 0.145
z = make_complex_par_vec(init=(1.0, 0.0))
#z[0].re = 1.0
#z[1].re = 1.0
#z[2].re = 1.0
#z[0].re = 0.5
#z[1].re = 0.5
#z[2].re = 0.5
z[0].re = 0.4
z[1].re = 0.6
z[2].re = 0.4
# Try to regulate to 1.73
#n01 = 1.338
#n21 = 1.338
#n01 = 1.11
#n21 = 1.11
n01 = 1.12
n21 = 1.12

for k in range(3):
	m.add_cout(v_p[k] - v_g[k] + z[k]*i_p[k])

m.add_cout(i_p[0] + i_p[1] + i_p[2])

for k in range(3):
	m.add_cout(p[k] - v_s[k]*fops.conjugate(i_s_ph[k]))

m.add_cout(-i_s[0] + i_s_ph[0] + i_s_ph[2])
m.add_cout(-i_s[1] - i_s_ph[1] - i_s_ph[0])
m.add_cout(-i_s[2] + i_s_ph[1] - i_s_ph[2])

m.add_cout(v_s[0] - v_s[1] - v_s[2])

m.add_cout(n01*(v_p[0] - v_p[1]) - v_s[0])
m.add_cout(n21*(v_p[2] - v_p[1]) - v_s[1])

m.add_cout(i_p[0] - n01*i_s[0])
m.add_cout(i_p[2] - n21*i_s[2])

#m.add_cout(i_p[0])
#m.add_cout(i_p[1] - 1)

s = fops.solver.SolverNewton(m)
k = s.solve(verbose=True)
print(k)

print('Primary voltages (l-n)')
print(extract_value(v_p[0], s.x).polar())
print(extract_value(v_p[1], s.x).polar())
print(extract_value(v_p[2], s.x).polar())

print('Secondary voltages (l-l)')
print(extract_value(v_s[0], s.x).polar())
print(extract_value(v_s[1], s.x).polar())
print(extract_value(v_s[2], s.x).polar())

print('Primary curents (l)')
print(extract_value(i_p[0], s.x).polar())
print(extract_value(i_p[1], s.x).polar())
print(extract_value(i_p[2], s.x).polar())

print('Secondary curents (l)')
print(extract_value(i_s[0], s.x).polar())
print(extract_value(i_s[1], s.x).polar())
print(extract_value(i_s[2], s.x).polar())
