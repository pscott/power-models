import math
import cmath
import matplotlib.pyplot as plt

def to_rad(deg):
	return deg*math.pi/180

def to_deg(rad):
	return rad*180/math.pi

class Generator(object):
	def __init__(self, w, L_pf, L_pp):
		self.w = w # angular frequency
		self.L_pf = L_pf # mutual inductance of field current (physically rotating) to phase voltage
		self.L_pp = L_pp # self-inductance phase winding (synchronous inductance)

	# i_ex exciter DC current
	# th_ro rotor angle (max emf point) relative to first winding 
	def phase_emf(self, i_ex, th_ro, phase):
		th_ex = -(2*math.pi/3)*phase - math.pi/2 + th_ro
		return self.w*1j*self.L_pf*cmath.rect(i_ex, th_ex)

	def phase_v(self, i_ex, th_ro, phase, i):
		emf = self.phase_emf(i_ex, th_ro, phase)
		return emf + self.w*1j*self.L_pp*i # remove phase winding drop

	def phase_i(self, i_ex, th_ro, phase, v):
		emf = self.phase_emf(i_ex, th_ro, phase)
		return (v - emf)/(self.w*1j*self.L_pp)

def create_arrow(x, name):
	theta = [0, cmath.phase(x)]
	rad = [0, abs(x)]
	return (theta, rad, name)

gen = Generator(w=2*math.pi*50, L_pf=0.001, L_pp=0.001)

# Find exciter current that produces emf a little greater than v
# Then rotate to increase power output
i_ex = 4
th_ro = to_rad(35)
#iss = [
#	cmath.rect(0, to_rad(-120*0)),
#	cmath.rect(0, to_rad(-120*1)),
#	cmath.rect(0, to_rad(-120*2)),
#	]
vs = [
	cmath.rect(1, to_rad(-120*0 - 0)),
	cmath.rect(1, to_rad(-120*1 - 0)),
	cmath.rect(1, to_rad(-120*2 - 0)),
	]

vals =  []
for ph in range(3):
	e = gen.phase_emf(i_ex, th_ro, ph)
	#i = iss[ph]
	#v = gen.phase_v(i_ex, th_ro, ph, i)
	v = vs[ph]
	i = gen.phase_i(i_ex, th_ro, ph, v)
	s = v*i.conjugate()
	vals.append({'ph': ph, 'e': e, 'i': i, 'v': v, 's': s})

arrows1 = []
for v in vals:
	arrows1.append(create_arrow(v['e'], 'e'+str(v['ph'])))
	#arrows1.append(create_arrow(v['v'], 'v'+str(v['ph'])))
	arrows1.append(create_arrow(v['i'], 'i'+str(v['ph'])))

arrows2 = []
for v in vals:
	arrows2.append(create_arrow(v['s'], 's'+str(v['ph'])))

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1, projection='polar')
ax2 = fig.add_subplot(2, 1, 2, projection='polar')

for ar in arrows1:
	ax1.plot(ar[0], ar[1], label=ar[2])

for ar in arrows2:
	ax2.plot(ar[0], ar[1], label=ar[2])

ax1.legend(loc='center left', bbox_to_anchor=(1.05, 0.5))
ax2.legend(loc='center left', bbox_to_anchor=(1.05, 0.5))

plt.show()
